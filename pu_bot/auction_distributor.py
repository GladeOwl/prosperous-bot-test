"""Track auctions that allow equal distribution and sale of materials."""

from typing import Optional

from hikari import Snowflake
from pydantic import validate_arguments
from sqlmodel import Session

from pu_bot.database.auction import get_bid
from pu_bot.models.auction import Auction, Bid


class DuplicateBid(Exception):
    """Raised when a duplicate bid is attempted"""


AuctionResult = dict[str, int]


@validate_arguments
def create_auction(  # pylint: disable=too-many-arguments
    guild_id: Snowflake,
    user_id: Snowflake,
    user_name: str,
    item: str,
    amount: int,
    price: float,
    location: str,
    currency: str,
) -> Auction:
    """Start an auction"""

    auction = Auction(
        guild_id=guild_id,
        creator_id=user_id,
        creator_name=user_name,
        item=item,
        amount=amount,
        price_per_item=price,
        location=location,
        currency=currency,
        message_id=None,
        channel_id=None,
    )

    return auction


def create_bid(
    session: Session,
    user_id: Snowflake,
    user_name: str,
    auction: Auction,
    max_amount: Optional[int] = None,
) -> Bid:
    """Create a new bid for an auction"""

    if get_bid(session, auction.auction_id, user_id) is not None:
        raise DuplicateBid("You can only bid on an auction once")

    bid = Bid(
        user_id=user_id,
        user_name=user_name,
        auction_id=auction.auction_id,
        max_amount=max_amount,
    )
    session.add(bid)

    return bid


def remove_bid(session: Session, user_id: Snowflake, auction: Auction) -> None:
    """Remove a user from an auction"""

    bid = get_bid(session, auction.auction_id, user_id)
    session.delete(bid)


def calculate_auction_results(user_id: Snowflake, auction: Auction) -> AuctionResult:
    """Finish an auction and calculate distribution of items to the bidders."""

    if user_id != auction.creator_id:
        raise ValueError("Only the creator of an auction can finish it")

    if not auction.bids:
        return {}

    for bid in auction.bids:
        if bid.max_amount is None:
            bid.max_amount = auction.amount

    auction_results: AuctionResult = {bid.user_name: 0 for bid in auction.bids}

    items_available = auction.amount
    resolved = False

    # Assign everyone the highest equal share, save remaining items
    bids = list(auction.bids)
    while not resolved:
        max_amount_per = items_available // (len(bids) or 1)
        if max_amount_per == 0:
            break

        for bid in list(bids):
            assert bid.max_amount is not None
            currently_given = auction_results[bid.user_name]

            amount_to_give = min(bid.max_amount - currently_given, max_amount_per)

            auction_results[bid.user_name] += amount_to_give
            items_available -= amount_to_give

            if auction_results[bid.user_name] >= bid.max_amount:
                bids.remove(bid)

            if items_available == 0 or len(bids) == 0:
                resolved = True
                break

    return auction_results


async def complete_auction(auction: Auction) -> str:
    """Finish out an auction."""

    results = calculate_auction_results(user_id=auction.creator_id, auction=auction)

    message = [
        (
            f"Auction on {auction.location} for {auction.amount} {auction.item}@{auction.price_per_item} "
            f"{auction.currency}/u results:"
        )
    ]
    for user_name, amount in results.items():
        message.append(
            f"{user_name}: {amount} {auction.item} "
            f"for a total of {amount*auction.price_per_item} {auction.currency}"
        )

    result = "\n* ".join(message)

    unallocated_items = auction.amount - sum(results.values())
    auction.complete = True
    return f"{result}\n\nUnallocated items: {unallocated_items} {auction.item}"
