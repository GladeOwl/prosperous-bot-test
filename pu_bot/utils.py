"""General purpose utilities for use throughout the bot."""

from hikari import Snowflake
from .bot import bot

# TODO: expire this at some point
USERNAME_CACHE = {}


async def get_username(guild_id: Snowflake, user_id: Snowflake) -> str:
    """Get the username for a user."""

    key = (guild_id, user_id)
    if key in USERNAME_CACHE:
        return USERNAME_CACHE[key]

    user = await bot.rest.fetch_member(guild_id, user_id)
    USERNAME_CACHE[key] = user.display_name
    return user.display_name
