""" Register commands and views for the bot. """

import logging
from typing import Optional

import hikari
import lightbulb
import miru
from hikari import Message, Snowflake

from pu_bot.auction_distributor import (
    Auction,
    DuplicateBid,
    complete_auction,
    create_auction,
    create_bid,
    remove_bid,
)
from pu_bot.database.auction import get_auction, get_auctions, get_bid
from pu_bot.database.session import DatabaseSession
from pu_bot.models.auction import Bid
from pu_bot.utils import get_username

from ..bot import bot

LOGGER = logging.getLogger(__name__)


class BidButton(miru.Button):
    """The button that triggers new bids to be added to the auction."""

    view: "AuctionView"

    def __init__(self, auction: Auction):
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Bid!",
            custom_id="bid_button_" + str(auction.auction_id),
        )
        self.auction_id = auction.auction_id

    async def callback(self, context: miru.ViewContext) -> None:
        """Create a new bid and update the message."""
        assert context.guild_id is not None
        with DatabaseSession() as session:
            auction = get_auction(session, self.auction_id)
            try:
                bid = create_bid(
                    session,
                    context.user.id,
                    await get_username(context.guild_id, context.user.id),
                    auction,
                )
                session.add(bid)
            except (
                DuplicateBid
            ):  # If user already registered as bidder, remove them from the auction
                remove_bid(session, context.user.id, auction)
            except ValueError as error:
                await context.respond(
                    content=str(error), flags=hikari.MessageFlag.EPHEMERAL
                )
                return

        await self.view.update_active_message()


class FinishButton(miru.Button):
    """Button that allows for completing an auction and calculating the results."""

    view: "AuctionView"

    def __init__(self, auction: Auction):
        super().__init__(
            style=hikari.ButtonStyle.SUCCESS,
            label="Finish",
            custom_id="finish_button_" + str(auction.auction_id),
        )
        self.auction_id = auction.auction_id

    async def callback(self, context: miru.ViewContext) -> None:
        """Finish out the auction attached to this view."""
        with DatabaseSession() as session:
            auction = get_auction(session, self.auction_id)
            if context.user.id != auction.creator_id:
                await context.respond(
                    content="Only the creator of an auction can finish it",
                    flags=hikari.MessageFlag.EPHEMERAL,
                )
                return

            await self.view.complete_auction()


class ChangeAmountButton(miru.Button):
    """Allows updating the maximum amount a bidder would like to receive from an auction."""

    view: "AuctionView"

    def __init__(self, auction: Auction):
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Change Bid Amount",
            custom_id="change_bid_amount_button_" + str(auction.auction_id),
        )
        self.auction_id = auction.auction_id

    async def callback(self, context: miru.ViewContext) -> None:
        # Find the bid with the user_id that matches context.user.id
        foundID = False
        with DatabaseSession() as session:
            auction = get_auction(session, self.auction_id)
            for bid in auction.bids:
                if bid.user_id == context.user.id:
                    modal = ChangeBidAmountModal(
                        auction=auction, user_id=bid.user_id, view=self.view
                    )
                    await context.respond_with_modal(modal)
                    foundID = True
        if not foundID:
            await context.respond(
                content="Please bid before you set a max amount",
                flags=hikari.MessageFlag.EPHEMERAL,
            )


class ChangeBidAmountModal(miru.Modal):
    def __init__(self, auction: Auction, user_id: Snowflake, view: "AuctionView"):
        super().__init__(
            title="Change Max Bid Amount",
            custom_id="ChangeBidAmountModal" + str(auction.auction_id),
        )
        self.auction_id = auction.auction_id
        self.user_id = user_id
        self.view = view

        # Set placeholder to the amount the max
        with DatabaseSession() as session:
            self.max_amount.placeholder = str(
                self.get_bid(session).max_amount or auction.amount
            )

    def get_bid(self, session) -> Bid:
        bid = get_bid(session, self.auction_id, self.user_id)
        assert (
            bid is not None
        ), f"Unable to locate bid on auction {self.auction_id} from user {self.user_id}"
        return bid

    max_amount = miru.TextInput(
        label="Max Amount",
        placeholder=("Max amount you'd like to receive"),
        required=True,
    )

    # The callback function is called after the user hits 'Submit'
    async def callback(self, ctx: miru.ModalContext) -> None:
        if self.max_amount.value is not None and self.max_amount.value.isdigit():
            with DatabaseSession() as session:
                bid = self.get_bid(session)
                bid.max_amount = int(self.max_amount.value)

            await self.view.update_active_message()
            await ctx.respond(
                content=(
                    f"Bid request maximum amount changed to {self.max_amount.value}"
                ),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
        else:
            await ctx.respond(
                content=(f"Please enter number."),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
        # You can also access the values using ctx.values, Modal.values, or use ctx.get_value_by_id()


class AuctionView(miru.View):
    """A view for the context."""

    def __init__(self, auction: Auction, *args, **kwargs) -> None:
        kwargs["timeout"] = None
        super().__init__(*args, **kwargs)

        self.auction_id = auction.auction_id
        self.add_item(BidButton(auction))
        self.add_item(FinishButton(auction))
        self.add_item(ChangeAmountButton(auction))

        self.cached_message: Optional[Message] = None

    async def update_active_message(self, message_owned: bool = True) -> str:
        """Update the message on the view to contain most recent information."""

        with DatabaseSession() as session:
            auction = get_auction(session, self.auction_id)

            new_message = (
                f"{auction.amount} {auction.item}@{auction.price_per_item:.2f} "
                f"{auction.currency}/u available on {auction.location}"
            )
            if len(auction.bids) > 0:
                new_message += "\nBids:"
            for bid in auction.bids:
                max_amount = bid.max_amount or auction.amount
                new_message += f"\n* {bid.user_name}: Up to {max_amount}"

            if message_owned:
                if self.cached_message is None:
                    assert auction.channel_id is not None
                    assert auction.message_id is not None
                    self.cached_message = await self.app.rest.fetch_message(
                        auction.channel_id, auction.message_id
                    )

                await self.cached_message.edit(content=new_message)
                self.cached_message.content = new_message

            return new_message

    async def complete_auction(self) -> None:
        """Close out the auction and display the results."""
        with DatabaseSession() as session:
            auction = get_auction(session, self.auction_id)
            message = await complete_auction(auction)

            if self.cached_message is None:
                assert auction.channel_id is not None
                assert auction.message_id is not None
                self.cached_message = await self.app.rest.fetch_message(
                    auction.channel_id, auction.message_id
                )

        await self.cached_message.edit(content=message, components=[])
        self.stop()

    async def on_timeout(self) -> None:
        """Close out the auction after a period of inactivity"""

        await self.complete_auction()


@bot.listen()
async def startup_views(_event: hikari.StartedEvent) -> None:
    """Load auctions from persistence on startup."""
    with DatabaseSession() as session:
        for auction in get_auctions(session):
            view = AuctionView(auction)
            if auction.channel_id is None or auction.message_id is None:
                LOGGER.warning(
                    f"Unable to reload {auction} due to missing channel and/or message ids"
                )
                continue
            await view.start(auction.message_id)


@bot.command
@lightbulb.command("ping", "Check if the bot is alive")
@lightbulb.implements(lightbulb.SlashCommand)
async def ping(ctx: lightbulb.Context) -> None:
    """Simple ping handler to test bot functionality"""

    await ctx.respond("Pong!")


@bot.command
@lightbulb.option(
    name="item",
    type=str,
    description="The item you want to create an auction for",
    required=True,
)
@lightbulb.option(
    name="amount",
    type=int,
    description="The amount of items you want to auction off",
    required=True,
)
@lightbulb.option(
    name="price",
    type=float,
    description="The price of each item you want to auction off",
    required=True,
)
@lightbulb.option(
    name="location",
    type=str,
    description="The location of the auction.",
    required=True,
)
@lightbulb.option(
    name="currency",
    type=str,
    description="The currency to use for the auction.",
    default="AIC",
    required=False,
)
@lightbulb.command("auction", "Start a new auction")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_new_auction(ctx: lightbulb.Context) -> None:
    """Create a new auction for an item."""

    assert (
        ctx.guild_id is not None
    ), "Auctions should only be created in the context of a guild"

    auction = create_auction(
        guild_id=ctx.guild_id,
        user_id=ctx.user.id,
        user_name=await get_username(ctx.guild_id, ctx.user.id),
        item=ctx.options.item,
        amount=ctx.options.amount,
        price=ctx.options.price,
        location=ctx.options.location,
        currency=ctx.options.currency,
    )

    view = AuctionView(auction)

    with DatabaseSession() as session:
        session.add(auction)
        session.commit()

        message = await ctx.respond(
            await view.update_active_message(False),
            components=view,
        )

        msg = await message.message()
        auction.message_id = msg.id
        auction.channel_id = msg.channel_id

    await view.start(message)
    await view.wait()


def create_message_link(
    guild_id: Snowflake, channel_id: Snowflake, msg_id: Snowflake
) -> str:
    return f"https://discord.com/channels/{guild_id}/{channel_id}/{msg_id}"


@bot.command
@lightbulb.command("list_auctions", "List all active auctions in the current server")
@lightbulb.implements(lightbulb.SlashCommand)
async def list_all_active_auction(ctx: lightbulb.Context) -> None:
    """List all available auctions"""
    assert (
        ctx.guild_id is not None
    ), "Auctions should only be listed in the context of a guild"

    response = []

    with DatabaseSession() as session:
        for auction in get_auctions(session):
            if auction.guild_id != ctx.guild_id:
                continue

            assert auction.channel_id is not None
            assert auction.message_id is not None

            link = create_message_link(
                auction.guild_id, auction.channel_id, auction.message_id
            )
            response.append(
                f"{auction.amount} {auction.item}@{auction.price_per_item:.2f} "
                f"{auction.currency}/u available on {auction.location}: {link}"
            )

        if not response:
            await ctx.respond("No active auctions.")
            return

        # TODO: handle messages that are too long
        await ctx.respond("\n".join(response))
