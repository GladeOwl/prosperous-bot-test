"""Module for defining commands that allow users to coordinate fulfillment of large or complex orders."""

import logging
from typing import Optional
from uuid import UUID
import hikari

import lightbulb
import miru
from hikari import Message
from miru.context import ViewContext

from pu_bot.models.order import OrderItemFulfiller
from pu_bot.utils import get_username

from ..bot import bot
from ..database.order import create_order, get_order, get_orders
from ..database.session import DatabaseSession

LOGGER = logging.getLogger(__name__)


class FulfillSomeModal(miru.Modal):
    """Modal to ask for how much someone wants to fulfill"""

    amount = miru.TextInput(
        label="Fulfill Amount",
        placeholder=("How much do you want to fulfill?"),
        required=True,
    )

    view: "OrderView"

    def __init__(self, view: "OrderView") -> None:
        super().__init__("Partial order fulfillment")

        self.view = view

    async def callback(self, context: miru.ModalContext) -> None:
        """Submit fulfillment"""

        item = self.view.order.items[0]

        remaining_amount = item.amount
        existing_fulfillment = None
        for fulfiller in item.fulfillers:
            if fulfiller.user_id == context.user.id:
                existing_fulfillment = fulfiller
            else:
                remaining_amount -= fulfiller.amount

        assert self.amount.value is not None
        try:
            fulfill_amount = int(self.amount.value)
        except TypeError:
            await context.respond(
                content="Invalid amount submitted", flags=hikari.MessageFlag.EPHEMERAL
            )
            return

        if fulfill_amount > remaining_amount:
            await context.respond(
                content=(
                    f"Fulfilling all remaining amount ({remaining_amount}) instead "
                    f"of requested amount ({fulfill_amount})"
                ),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            fulfill_amount = remaining_amount

        assert context.guild_id is not None

        if existing_fulfillment:
            existing_fulfillment.amount = fulfill_amount
        else:
            item.fulfillers.append(
                OrderItemFulfiller(  # type: ignore
                    user_id=context.user.id,
                    user_name=await get_username(context.guild_id, context.user.id),
                    amount=fulfill_amount,
                )
            )

        self.view.session.commit()
        await self.view.update_message()
        await context.respond(
            content=(
                f"Fulfillment of {fulfill_amount} {item.item} submitted successfully"
            ),
            flags=hikari.MessageFlag.EPHEMERAL,
        )


class FulfillSomeButton(miru.Button):
    """Button to enable people to fulfill a portion of an order."""

    view: "OrderView"

    def __init__(self, order_id: str) -> None:
        super().__init__(label="Fulfill Some", custom_id=f"fulfill_some_{order_id}")

    async def callback(self, context: ViewContext):
        """Respond with a modal to allow for a set number of items to be fulfilled"""

        modal = FulfillSomeModal(self.view)

        await context.respond_with_modal(modal)


class FulfillAllButton(miru.Button):
    """Button to enable people to fulfill the entirety of an order."""

    view: "OrderView"

    def __init__(self, order_id: str) -> None:
        super().__init__(label="Fulfill All", custom_id=f"fulfill_all_{order_id}")

    async def callback(self, context: ViewContext) -> None:
        """Trigger complete fulfillment from a single user."""

        item = self.view.order.items[0]

        remaining_amount = item.amount
        existing_fulfillment = None
        for fulfiller in item.fulfillers:
            remaining_amount -= fulfiller.amount
            if fulfiller.user_id == context.user.id:
                existing_fulfillment = fulfiller

        assert context.guild_id is not None

        if existing_fulfillment:
            existing_fulfillment.amount += remaining_amount
        else:
            item.fulfillers.append(
                OrderItemFulfiller(  # type: ignore
                    user_id=context.user.id,
                    user_name=await get_username(context.guild_id, context.user.id),
                    amount=remaining_amount,
                )
            )
        self.view.session.commit()
        await self.view.update_message()


class OrderView(miru.View):
    """A view for displaying the status of an order."""

    @classmethod
    async def new(
        cls,
        item: str,
        amount: int,
        price: int,
        currency: str,
        location: str,
        ctx: lightbulb.Context,
    ) -> "OrderView":
        """Create a new OrderView, ensuring that it is persisted into the database.

        This constructor also handles creating the initial response, sending it, starting the view,
        and linking that message to the Order in the database.

        This way, it is impossible to end up with an OrderView that isn't persisted or linked
        to the message properly.
        """
        # Create the database order
        session = DatabaseSession()
        order = await create_order(
            session, item, amount, price, currency, location, ctx
        )
        session.commit()

        view = cls(order.order_id)

        # Generate message
        response = await ctx.respond(view.generate_message(), components=view)
        message = await response.message()

        await view.start(message)

        order.channel_id = message.channel_id
        order.message_id = message.id
        session.commit()

        return view

    def __init__(
        self, order_id: UUID, session: Optional[DatabaseSession] = None
    ) -> None:
        super().__init__(timeout=None)

        self.session = session or DatabaseSession()
        self.order = get_order(self.session, order_id)

        self.add_item(FulfillAllButton(str(order_id)))
        self.add_item(FulfillSomeButton(str(order_id)))

    async def get_message(self) -> Message:
        """Get the message associated with this view."""

        if self._message is None:
            # Lazy load message if it is not yet loaded
            assert self.order.channel_id is not None
            assert self.order.message_id is not None
            self._message = await self.bot.rest.fetch_message(
                self.order.channel_id, self.order.message_id
            )

        return self._message

    def stop(self) -> None:
        """Make sure database session is closed when view is stopped."""
        super().stop()
        self.session.close()

    def generate_message(self) -> str:
        """Generate a message content based on current state of the order"""

        # TODO: handle multiple items once orders are capable of supporting them
        # Currently we're only limited by discord's poor UI components
        assert len(self.order.items) == 1

        order_item = self.order.items[0]

        header = (
            f"Looking for {order_item.amount} {order_item.item} for "
            f"{order_item.price_per_item:.2f} {order_item.currency}/u on {order_item.location}"
        )

        message = [header]

        has_fulfillers = False
        for fulfiller in order_item.fulfillers:
            has_fulfillers = True
            message.append(
                f"* {fulfiller.user_name} - {fulfiller.amount} {order_item.item} - "
                f"{fulfiller.total_price:.2f} {order_item.currency}"
            )
        if has_fulfillers:
            message.append(f"{order_item.amount_remaining} remaining items needed")

        return "\n".join(message)

    async def update_message(self) -> None:
        """Update the message with the latest state."""

        message = await self.get_message()

        if self.order.items[0].fulfilled:
            await message.edit(self.generate_message(), components=[])
            self.stop()
        else:
            await message.edit(self.generate_message())


@bot.command
@lightbulb.option(
    name="item",
    type=str,
    description="The item you want to create an order for",
    required=True,
)
@lightbulb.option(
    name="amount",
    type=int,
    description="The amount of items you want to order",
    required=True,
)
@lightbulb.option(
    name="price",
    type=float,
    description="The price of each item you want to order",
    required=True,
)
@lightbulb.option(
    name="location",
    type=str,
    description="The location you want to pick up at",
    required=True,
)
@lightbulb.option(
    name="currency",
    type=str,
    description="The currency you want to spend for this order",
    default="AIC",
    required=False,
)
@lightbulb.command("order", "Create a new order")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_new_order(ctx: lightbulb.Context) -> None:
    """Create a new order to find people who can fulfill it"""

    view = await OrderView.new(
        ctx.options.item,
        ctx.options.amount,
        ctx.options.price,
        ctx.options.currency,
        ctx.options.location,
        ctx,
    )

    LOGGER.info(f"New order created: {view.order.order_id}")

    await view.wait()


@bot.listen()
async def reload_orders(_event: hikari.StartedEvent) -> None:
    """Reload orders from the database."""

    with DatabaseSession() as session:
        for order in get_orders(session):
            view = OrderView(order_id=order.order_id)
            await view.start(order.message_id)
