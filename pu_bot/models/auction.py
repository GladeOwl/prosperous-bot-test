"""Module that defines sql models to use with auctions."""

from typing import List, Optional
from uuid import UUID, uuid4

from hikari import Snowflake
from sqlmodel import Field, Relationship, SQLModel


class Auction(SQLModel, table=True):
    """Tracks a specific instance of an auction."""

    auction_id: UUID = Field(default_factory=uuid4, primary_key=True)
    message_id: Optional[Snowflake]
    channel_id: Optional[Snowflake]
    guild_id: Snowflake
    creator_id: Snowflake
    creator_name: str
    item: str
    amount: int
    price_per_item: float
    location: str
    currency: str

    complete: bool = Field(default=False, index=True)

    bids: List["Bid"] = Relationship(back_populates="auction")

    def __str__(self) -> str:
        string = (
            f"{'Completed' if self.complete else 'Open'} auction for {self.amount} "
            f"{self.item} for {self.price_per_item} {self.currency} each on {self.location} "
            f"by {self.creator_name}"
        )
        return string


class Bid(SQLModel, table=True):
    """Tracks a specific bid on an auction"""

    auction_id: UUID = Field(foreign_key="auction.auction_id", primary_key=True)
    user_id: Snowflake = Field(primary_key=True)
    user_name: str
    max_amount: Optional[int]

    auction: Auction = Relationship(back_populates="bids")

    def __str__(self) -> str:
        return f"{self.user_name} - {self.max_amount or self.auction.amount}"
