"""Run the bot."""

from pu_bot.bot import bot

import pu_bot.commands  # pylint: disable=unused-import

bot.run()
