"""Module for auction related database operations"""

from typing import List, Optional
from uuid import UUID
from hikari import Snowflake
from sqlmodel import Session, select
from pu_bot.models.auction import Auction, Bid


def get_auctions(session: Session, include_completed: bool = False) -> List[Auction]:
    """Get all auctions, optionally filtering out completed auctions (filtered by default)"""

    query = select(Auction)
    if not include_completed:
        query = query.filter_by(complete=False)

    return session.exec(query).all()


def get_auction(session: Session, auction_id: UUID) -> Auction:
    """Load an auction from the database."""

    query = select(Auction).filter_by(auction_id=auction_id)
    return session.exec(query).one()


def get_bids(session: Session, auction_id: UUID) -> List[Bid]:
    """Get bids for an auction"""

    query = select(Bid).filter_by(auction_id=auction_id)

    return session.exec(query).all()


def get_bid(session: Session, auction_id: UUID, user_id: Snowflake) -> Optional[Bid]:
    """Get bids for an auction"""

    query = select(Bid).filter_by(auction_id=auction_id)

    if user_id is not None:
        query = query.filter_by(user_id=user_id)

    return session.exec(query).one_or_none()
