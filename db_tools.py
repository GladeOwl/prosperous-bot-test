"""A simple script to load auctions from json into the database."""
import asyncio
import json

from sqlmodel import SQLModel, select

from pu_bot.database import DatabaseSession
from pu_bot.database.auction import get_auctions
from pu_bot.database.session import ENGINE
from pu_bot.models import Auction, Bid
from pu_bot.bot import bot
from pu_bot.utils import get_username


async def main():
    """Load json data from disk and insert into the database."""
    SQLModel.metadata.create_all(ENGINE)
    await bot.start()

    report_auctions()
    dump_auctions("original.json")
    await correct_auctions()
    report_auctions()
    dump_auctions("updated.json")


def load_auctions(file_name: str):
    """Load auctions from json into the database"""
    with open(file_name, "r", encoding="utf-8") as file:
        auction_list = json.load(file)

    for _auction_id, auction_info in auction_list.items():
        bids = auction_info.pop("bids")
        auction = Auction.parse_obj(auction_info)
        auction.bids = [Bid.parse_obj(bid_info) for bid_info in bids]

        yield auction


def dump_auctions(file_name: str):
    """Dump out auctions from DB into a json file."""
    with DatabaseSession() as session:
        auctions = get_auctions(session, include_completed=True)

        data = {}
        for auction in auctions:
            json_data = json.loads(auction.json())
            json_data["bids"] = [json.loads(bid.json()) for bid in auction.bids]
            data[str(auction.auction_id)] = json_data

    json.dump(data, open(file_name, "w"))


async def correct_auctions():
    """Check for which auctions are completed and which ones aren't"""

    with DatabaseSession() as session:
        auctions = get_auctions(session, include_completed=True)

        for auction in auctions:
            assert auction.channel_id is not None
            assert auction.message_id is not None
            message = await bot.rest.fetch_message(
                auction.channel_id, auction.message_id
            )

            assert message.content is not None
            if "results" in message.content:
                if auction.complete:
                    print(f"Completed auction found: {auction}")
                else:
                    print(f"Missed completed auction in database: {auction}")
                    auction.complete = True
            else:
                if auction.complete:
                    print(f"Completed auction that wasn't resolved found: {auction}")
                    auction.complete = False
                else:
                    print(f"Open auction found: {auction}")

            if auction.creator_name is None:
                print("Loading username for auction")
                username = await get_username(auction.guild_id, auction.creator_id)
                auction.creator_name = username

            for bid in auction.bids:
                if bid.user_name is None:
                    print("Loading username for bid")
                    try:
                        username = await get_username(auction.guild_id, bid.user_id)
                    except Exception as error:
                        print(f"Unable to locate username for {bid.user_id}: {error!r}")
                    else:
                        bid.user_name = username


def report_auctions():
    """Report the auctions that are in the database"""
    with DatabaseSession() as session:
        query = select(Auction)
        auctions = sorted(
            session.exec(query).all(), key=lambda auction: auction.complete
        )
        print(f"{len(auctions)} auctions loaded:")
        for auction in auctions:
            print(auction, *auction.bids, sep="\n* ", end="\n\n")


if __name__ == "__main__":
    asyncio.run(main())
