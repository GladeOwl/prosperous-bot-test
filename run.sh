source ./secrets.sh


# Take a backup of the database before running migration
touch pu_bot.db
cp pu_bot.db backup.db

# Run alembic migration
pushd pu_bot/alembic
poetry run alembic upgrade head
popd

poetry run python -m pu_bot